#! /usr/bin/bash
#
# Provisioning script common for all servers

set -o errexit # abort on nonzero exitstatus
set -o nounset # abort on unbound variable

#
# Variables
#

# Location (on the VM) of provisioning scripts and related files
provisioning_files=/vagrant/provisioning

#
# Package installation
#
# sudo yum install -y  with contents of file "${provisioning_files}/common-packages" as argument

#
# Admin user
#

